;;; twittering-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (twit) "twittering-mode" "twittering-mode.el" (21312
;;;;;;  15731 722203 659000))
;;; Generated autoloads from twittering-mode.el

(autoload 'twit "twittering-mode" "\
Start twittering-mode.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("twittering-mode-pkg.el") (21312 15731
;;;;;;  735844 87000))

;;;***

(provide 'twittering-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; twittering-mode-autoloads.el ends here
